---
layout: post
title: "IE에서 발생하는 CSS 모션 이슈 정리 | 하만"
author: Jaden
tags: [issue, harman, css, background, transform, transition, unit, ie, cross-browsing]
comments: true
---

이 포스팅은 Harman 프로젝트를 진행하면서 발생했던 CSS 이슈에 대해 정리한 글입니다.

추후 프로젝트 진행하면서 생기는 추가적인 이슈들 또한 취합하여 꾸준히 업로드하도록 하겠습니다.

## 1. IE 에서 단위 혼합 시 발생하는 Transition 이슈

\<발생 이슈\>
![screensh](../images/transform-motion-issues-gif-04.gif){: width="300px"}

<!--
<div class="center">
    <img src="../images/transform-motion-issues-gif-03.gif" alt="정상 화면" style="display:inline-block;width:300px;">
    <img src="../images/transform-motion-issues-gif-04.gif" alt="이슈 화면" style="display:inline-block;width:300px;">
</div>
-->

해당 화면은 하만 프로젝트 작업 도중에 발생한 이슈입니다.

스크롤을 내린 이후에 렉시콘 이미지가 오른쪽에서 왼쪽으로 지나가다가 가운데로 위치해야 하는데, IE에서 위 화면처럼 빠르게 휙 사라지는 현상이 있었습니다. ~~(심지어 방향도 반대로..)~~

해당 소스를 살펴보면 다음과 같습니다.

```scss
    $mo : "screen and (max-width:768px)";
    @mixin motion-srrr($x:0px,$y:40px,$duration:.35s,$easing:ease-out,$opacity:0) {
      @if $opacity == 0 {visibility:hidden;opacity:0;}
      @else {opacity:$opacity;}
      will-change:transform, opacity;
      transform: translate($x,$y);
      &.yonom{visibility:visible; opacity:1; transform: translate(0,0); transition-property:all,opacity;transition-duration:$duration;transition-timing-function:$easing,linear;}
      @media #{$mo} {
        transition-timing-function:ease-out, linear;
      }
    }
```

`yonom` 이라는 클래스를 추가할 시 모션을 일으키는데 `transform:translate`에 사용되어지는 단위가 기본값은 `px` 로 설정되어있지만 `%, vw` 등 여러가지가 사용될 수 있고, 도착지점은 (0, 0) 입니다. 

CanIUse[^1] 에서 확인 시 `vw` 단위는 많은 이슈를 가지고 있고, 확인 끝에 IE 에서는 트랜지션을 일으키는데 다른 단위와 혼합 사용 시 호환이 좋지 못한 것을 알 수 있었습니다.[^2]

실제 작업하면서 관련 값의 단위를 다르게 놓는 경우는 거의 없기도 하고 위 소스에서도 다른 단위와 섞어놓지 않았지만, __IE 개발자 도구에서 `transform:translate` 에서 0 이라는 수치에 단위값을 기본으로 `px`로 변환__ 하기 때문에 다른 단위를 쓰지 않고도 0 이라는 수치로 인해 `?vw ~ 0px` 의 이동을 일으켜 버그가 발생한 것을 알 수 있었습니다.

때문에 해당 소스를 다음과 같이 수정하였습니다.

```scss
/* 1. $unit 추가 */
    @mixin motion-srrr($x:0px,$y:40px,$duration:.35s,$easing:ease-out,$opacity:0,$unit:'px') {
    
/* 2. 단위에 따른 별도 처리 */
    @if $unit == 'vw' {&.yonom{transform: translate(0.01vw,0.01vw);}}
    @else {&.yonom{transform: translate(0,0);}}
```

이후 완성된 소스는 다음과 같습니다.

```scss
    $mo : "screen and (max-width:768px)";
    @mixin motion-srrr($x:0px,$y:40px,$duration:.35s,$easing:ease-out,$opacity:0,$unit:'px') {
      @if $opacity == 0 {visibility:hidden;opacity:0;}
      @else {opacity:$opacity;}
      will-change:transform, opacity;
      transform: translate($x,$y);
      @if $unit == 'vw' {&.yonom{transform: translate(0.01vw,0.01vw);}}
      @else {&.yonom{transform: translate(0,0);}}
      &.yonom{visibility:visible; opacity:1; transition-property:all,opacity;transition-duration:$duration;transition-timing-function:$easing,linear;}
      @media #{$mo} {
        transition-timing-function:ease-out, linear;
      }
    }
```

`$unit` 이라는 단위 체크를 따로 반영하여 `vw` 단위를 가지는 경우에는 `0.01vw` 로 따로 구분해 두었습니다.

_(0vw로 할 경우 css transpiling 과정에서 다시 단위를 제외한 0 값을 추출해주기 때문에 임계값으로 설정)_

추후 작업에서도 __트랜지션을 가지는 영역에서 0인 경우에도 단위까지 적어주고, 빌드 과정을 거치는 파일에는 0.01vw 처럼 유사값을 작성__ 하여 이후 작업 시 차질이 없길 바랍니다.

\<처리 결과\>
![screensh](../images/transform-motion-issues-gif-03.gif){: width="300px"}

## 2. background-attachment:fixed 끊김 현상

<div class="center">
    <p style="display:inline-block;width:45%;">
        <img src="../images/transform-motion-issues-gif-01.gif" alt="하만소개 - 하만 정상 화면">
        <span>[크롬 화면]</span>
    </p>
    <p style="display:inline-block;width:45%;">
        <img src="../images/transform-motion-issues-gif-02.gif" alt="하만소개 - 하만 깨지는 화면">
        <span>[IE 화면]</span>
    </p>
</div>

이 또한 하만 프로젝트에서 나타난 IE 브라우저 이슈입니다.

스크롤 시 타 브라우저에서는 배경이 고정된 채로 노출이 되는 것을 확인할 수 있는데, IE 에서는 스크롤 시 덜그럭거리는 현상이 발견되었습니다.

해당 소스를 살펴보면 다음과 같습니다.

```scss
    .about-harman .about-full-img {
        position:relative;
        width:100%;
        @include vw-pc(height,1200);
        background-image:url("//images.samsung.com/is/image/samsung/p5/sec/macrosite/brand_harman/about/about-harman-intro-full.jpg?$ORIGIN_JPG$");
        background-repeat:no-repeat;
        background-position:50% 48%;
        background-attachment:fixed;
        @include vw-pc(background-size,auto 800);
    }
```

`background-attachment` 의 값이 `fixed` 인 경우 IE에서 버그가 발생한다는 내용은 인터넷에 검색해보면 쉽게 알 수 있는데, 해당 문제를 해결하는 방법을 찾아 보았습니다.

### 1. css를 통한 제어
```css
    html{overflow: hidden;}
    body{overflow: auto; height: 100%;}
```

해당 방법은 기본 스크롤를 막았기 때문에 이외의 스크롤 이벤트가 필요한 페이지에서는 적용할 수 없습니다.

하만 프로젝트에서도 `sticky` 기능을 사용하기 때문에 이는 사용할 수 없었습니다.

### 2. js를 통한 제어
```js
    if(navigator.userAgent.match(/Trident\/7\./)) {
      document.body.addEventListener("mousewheel", function() {
        event.preventDefault();
        var wd = event.wheelDelta;
        var csp = window.pageYOffset;
        window.scrollTo(0, csp - wd);
      });
    }
```

해당 사항은 마우스 휠을 스크립트를 통해 제어를 하는 것인데 키보드 제어는 못하는 문제가 있습니다.

### 3. 인터넷 옵션 변경

![screensh](../images/transform-motion-issues-png-01.png){: width="400"}

실제로 이 문제는 위 이미지와 같이 __`인터넷 옵션 > 고급 탭 > 부드러운 화면 이동 사용` 에 체크를 해제__ 하는 것으로 해결이 가능합니다.

다만 전체적인 스크롤 제어가 뚝뚝 끊겨보이는 현상이 있을 수 있고, 각 사용자들이 설정을 건드리는 방법은 맞지 않습니다.

결국, 해당 현상이 보이는 운영체제 및 브라우저에서는 static 화면으로 노출되도록 class 삽입 처리를 하였습니다.

```js
/* window10 ie11에서 background fixed 버벅거림 문제로 인해 해당 모션이 적용된 부분 static 처리(attach-scroll 클래스 추가) */
    const _ie = (window.navigator.appName === 'Netscape' && window.navigator.userAgent.toLowerCase().indexOf('trident') !== -1) || (window.navigator.userAgent.toLowerCase().indexOf("msie") !== -1);
    if (_ie) {
        const winVersion = parseInt(window.navigator.userAgent.split('NT')[1].split(';'));
        if (winVersion === 10) {
            $('#pageAboutHarman').find('.about-full-img').addClass('attach-scroll');
            $('#pageHighEndMarkLevinson').find('.highend-product-mark .sec-1').addClass('attach-scroll');
            $('#pageHighEndRevel').find('.highend-product-revel .sec-4').addClass('attach-scroll');
        }
    }
```

---

[^1]:<https://caniuse.com/#feat=viewport-units>
[^2]:<https://stackoverflow.com/questions/30813193/pseudo-elements-transition-on-ie-11-ignores-padding/32852501>