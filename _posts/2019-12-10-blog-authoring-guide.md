---
layout: post
title:  "Blog Authoring Guide : MTG-3T Project & Tech Issue"
author: Jaden
tags: [blog, post, project, tech, issue guide]
comments: true
---

# INDEX

[1. 제작의도](#1-제작의도)

[2. 블로그 개요](#2-블로그-개요)

[3. 블로그 작성 가이드](#3-블로그-작성-가이드)

---

# 1. 제작의도
## 팀내 프로젝트 이슈 공유
### 프로젝트 이슈란?
- 프로젝트를 진행하면서 생기는 버그 & 특이사항 & 팁
- 프로젝트에 수행하는데 있어 필요한 지식
- 해결되지 않았거나 논의가 필요한 이슈사항
### 프로젝트 이슈 공유로 얻는것들
- 동일한 이슈에 대한 시행착오 방지
- 인수인계 용이
- 지식 나눔을 통한 동반 성장

## 사내 기술 블로그 추진
### 기술블로그란?
- IT트렌드 혹은 실제 사용하고 있는 소프트웨어 및 개발 관련하여 양질의 컨텐츠와 기술의 정보를 공유
- 해당 컨텐츠에 대한 실무자들간의 소통
- 네이버, 카카오 등 주요 IT 기업에서는 이미 기술 블로그가 활성화 됨
### 주요 IT 기업 기술 블로그

- [네이버D2](https://d2.naver.com){:target="_blank"}
- [LINE](https://engineering.linecorp.com/ko/blog/){:target="_blank"}
- [NTS](https://wit.nts-corp.com/){:target="_blank"}
- [카카오](https://tech.kakao.com/blog/){:target="_blank"}
- [우아한형제들](http://woowabros.github.io/){:target="_blank"}
- [레진](https://tech.lezhin.com){:target="_blank"}

### 사내 기술 블로그를 통해 얻는것들
- 해당 기업의 기술 역량 어필
- 사내 IT팀원들과 기술적 교류
- 각 직군에 대한 이해도 향상


# 블로그 개요
## 블로그 제작
- Jekyll[^1], hexo 등 블로그 프레임워크 설치
- Jekyll Themes, hexo Themes 등 템플릿 디자인 적용 및 커스텀 제작
- Discus, Livere 등 소셜 댓글 서비스 적용
- MarkDown[^2]을 통하여 포스팅 제작
- Gitlab, Github 등 블로그 프로젝트 저장소에 배포

## 블로그 구성
- 포스트 : 게시글을 통해 내용 전달
- 댓글 : 게시글에 대한 반응 및 피드백을 전달
- 아카이브 : 날짜별로 게시글의 리스트를 한 눈에 볼 수 있음
- 태그 : 찾고 싶은 키워드(태그)를 선택하여 게시글 검색


# 블로그 작성 가이드
## 작업환경 세팅
1. GITLAB[^3] 회원가입 (가입 이후 Jaden에게 ID 전달)
2. 해당 블로그 저장소[^4] 클론 내려받기
3. 루비 설치
    1) rbenv 패키지관리자 설치
    - `brew update`
    - `brew upgrade rbenv ruby-build`
    2) ruby 설치
    - `rbenv install 2.6.3`
    - `rbenv global 2.6.3`
4. 환경변수 설정
    1) `sudo vi ~/.bash_profile`
    2) 암호 입력 후 `i` 키 입력하여 쓰기 활성화하여 아래 문구 붙여넣기
    3) `if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi`
    4) Esc 키 누르고 `:wq` 입력
5. 번들러 설치
    1) 와이파이로 연결 (사내망 사용 불가)
    2) SSL 이슈로 인해 http로 경로 전환하여 설치
    3) `gem install bundler –source http://rubygems.org`
6. 내려받은 지킬 디렉토리로 이동하여 Gemfile 에 기록된 젬(Gem) 설치
    - `bundle install`


## 포스팅

1. /_post/ 폴더 안에 md 확장자로 파일을 생성 (`yyyy-mm-dd-제목.md`)

2. 파일 상단에 다음과 같은 파일 정보 입력
    1) Layout : 해당 게시글을 노출시킬 레이아웃 설정 (post로 통일)
    2) Title : 블로그 리스트에 노출될 제목 문구
    3) Author: 작성자 (영어이름으로 통일)
    4) Tags : 해당 게시글을 노출시킬 태그 삽입
    5) Comments : 댓글기능유무 (true로 통일)

3. 마크다운을 통하여 작성
    - 기본적으로는 마크다운으로 작성하되, 필요시 HTML 마크업으로도 사용 가능
    - 이미지 사용 시 왼쪽 정렬이 기본이며, 가운데 정렬이 필요한 경우 아래 코드를 참고하여 별도 처리

4. GIF 편집
    1) App Store에서 gif brewery 를 검색
    2) GIF Brewery 3 설치 및 실행
    3) Record Screen 을 통하여 화면 녹화 (QuickTime Player, 반디캠 등에서도 가능)
    4) Resize, Crop, Canvas 조절 및 프레임 세팅 등 편집 작업 후  Create 버튼으로 생성

5. CodePen 적용
    1) CodePen[^5] 접속 후 회원가입
    2) 왼쪽 메뉴에서 Pen 클릭
    3) 사용할 소스를 각 탭(html/css/javascript)에 알맞게 작성
    4) Save 버튼을 누르면 주소가 생성되며 우측 하단에 메뉴 노출
    5) 우측 하단에 Embed 버튼 클릭
    6) Copy & Paste Code 란에 있는 코드를 복사하여 적용할 포스팅에 붙여넣기

## 빌드 및 배포
1. 로컬 빌드 : /_site/ 폴더 생성
    - `bundle exec Jekyll build`
2. 로컬 서버 구현 : http://localhost:4000 생성
    - `bundle exec jekyll serve`
3. 배포 (저장소 : https://gitlab.com/mtg3t/mtg3t.gitlab.io.git)
    - 신규 및 변경 포스팅 파일, 레이아웃 수정 등 Commit & Push
    - 저장소 페이지의 [CI/CD] – [Pipeline] 메뉴에서 빌드 완료 여부 확인 가능


[^1]:<https://jekyllrb-ko.github.io/>
[^2]:<https://heropy.blog/2017/09/30/markdown/#makeudaun-munbeob-syntax>
[^3]:<https://www.gitlab.com>
[^4]:<https://gitlab.com/mtg3t/mtg3t.gitlab.io.git>
[^5]:<https://codepen.io/>

