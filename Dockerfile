FROM jekyll/jekyll:3.4

WORKDIR /srv/jekyll

RUN gem install jekyll-paginate -v "~>1.1.0"
RUN gem install public_suffix -v "4.0.1"
RUN gem install addressable -v "2.7.0"
RUN gem install ffi -v "1.11.3"
RUN gem install rb-fsevent -v "0.10.3"
RUN gem install rb-inotify -v "0.10.0"
RUN gem install sass -v "3.7.4"
RUN gem install jekyll-sass-converter -v "1.5.2"
RUN gem install listen -v "3.2.1"
RUN gem install jekyll-watch -v "1.5.1"
RUN gem install kramdown -v "1.17.0"
RUN gem install pathutil -v "0.16.2"
RUN gem install safe_yaml -v "1.0.5"
RUN gem install jekyll -v "3.4.0"

CMD ['bundler', 'install']

EXPOSE 4000

